var ctx = document.getElementById("barChart").getContext("2d");
var barChart = new Chart(ctx, {
    type: "bar",
    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
                label: "Borrowed",
                background: "rgba(39, 58, 72, 0.4)",
                borderColor: "rgba(255, 0, 0, 0.5)",
                borderWidth: 2,
                data: [10, 20, 30, 10, 100, 50, 10],
            },
            {
                label: "Returned",
                background: "rgba(39, 58, 72, 0.4)",
                borderColor: "rgba(69, 182, 254, 0.8)",
                borderWidth: 2,
                data: [40, 20, 10, 5, 70, 25, 30],
            },
        ],
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx2 = document.getElementById("pieChart").getContext("2d");
var pieChart = new Chart(ctx2, {
    type: "doughnut",
    data: {
        labels: ["Romance", "Mystery", "Fiction", "Fantasy", ],
        datasets: [{
            data: [5, 2, 4, 6],
            backgroundColor: [
                "rgba(255, 1, 1, 0.7)",
                "rgba(255, 245, 0, 0.7)",
                "rgba(0, 193, 43, 0.7)",
                "rgba(0, 133, 255, 0.7)",
            ],
        }, ],
    },

});