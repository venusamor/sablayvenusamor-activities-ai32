import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "./../components/pages/Dashboard.vue";
import PatronManagement from "./../components/pages/PatronManagement.vue";
import BookManagement from "./../components/pages/BookManagement.vue";
import Settings from "./../components/pages/Settings.vue";


Vue.use(VueRouter);

export default new VueRouter({
    mode: "history",
    routes: [{
            path: "/",
            component: Dashboard,
        },
        {
            path: "/Dashboard",
            name: "Dashboard",
            component: Dashboard,
        },
        {
            path: "/PatronManagement",
            name: "PatronManagement",
            component: PatronManagement,
        },

        {
            path: "/BookManagement",
            name: "BookManagement",
            component: BookManagement,
        },

        {
            path: "/Settings",
            name: "Settings",
            component: Settings,
        },

    ],
});