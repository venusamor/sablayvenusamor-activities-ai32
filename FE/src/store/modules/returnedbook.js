import axios from "../../config";
export default {
    state: {
        returned: [],
    },
    mutations: {
        setRetunedBooks: (state, returned) => (state.returned = returned),
    },
    getters: {
        getReturnedBooks: (state) => state.returned,
    },
    actions: {
        async fetchReturnedBook({ commit }) {
            await axios
                .get("returnedbooks")
                .then((res) => {
                    commit("setReturnedBooks", res.data);
                })
                .catch((err) => {
                    console.log(err);
                });
        },
        async ReturnBook({ commit }, book) {
            const response = await axios
                .post("returnedbooks", book)
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });
            return response;
        },
    },
};