import axios from "../../config";

export default {
    state: {
        borrowedbooks: [],
    },
    getters: {
        getBorrowedBooks: (state) => state.borrowedbooks,
    },
    mutations: {
        setBorrowedBooks: (state, borrowedbooks) =>
            (state.borrowedbooks = borrowedbooks),
    },

    actions: {
        async fetchBorrowedBooks({ commit }) {
            await axios
                .get("borrowedbooks")
                .then((res) => {
                    commit("setBorrowedBooks", res.data);
                })
                .catch((err) => {
                    console.error(err);
                });
        },
        async BorrowBook({ commit }, book) {
            const response = await axios
                .post("borrowedbooks", book)
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });
            return response;
        },
    },
};