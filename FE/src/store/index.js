import Vue from 'vue';
import Vuex from 'vuex';
import books from './modules/books'
import patrons from './modules/patrons'
import borrowedbook from './modules/borrowedbook'
import returnedbook from './modules/returnedbook'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        books,
        patrons,
        borrowedbook,
        returnedbook
    }
});