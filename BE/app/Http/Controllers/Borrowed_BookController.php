<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBorrowedBookRequest;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\borrowed_book;

class Borrowed_BookController extends Controller
{
    public function index(){
        return response()->json(borrowed_book::with(['patron', 'book', 'book.category'])->get());
    }

    /**
     * Stores borrowed_book.
     * 
     */
    public function store(StoreBorrowedBookRequest $request){
        $borrowedanother = borrowed_book::create($request->only(['book_id', 'copies', 'patron_id']));
        $borrowed_book = borrowed_book::with(['book'])->find($borrowedanother->id);
        $copies = $borrowed_book->book->copies - $request->copies;
        $borrowed_book->book->update(['copies' => $copies]);
        return response()->json(['borrowed_book' => $borrowed_book]);
    }
}
