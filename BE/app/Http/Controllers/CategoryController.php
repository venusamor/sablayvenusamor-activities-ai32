<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Retrieves all categories.
     * 
     */
    public function index(){
        return response()->json(Category::all());
    }
}
