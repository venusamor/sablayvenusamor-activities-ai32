<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreReturnedBookRequest;
use Illuminate\Http\Request;
use App\Models\borrowed_book;
use App\Models\returned_book;
use function PHPUnit\Framework\isEmpty;
class Returned_BookController extends Controller
{
    public function index()
    {
        return response()->json(returned_book::with(['book', 'patron', 'book.category'])->get());
    }

    public function store(StoreReturnedBookRequest $request)
    { 
        $borrowed = borrowed_book::where([
            ['book_id', $request->book_id],
            ['patron_id', $request->patron_id]])->firstOrFail();
        
        if(!empty($borrowed))
        {
            if($borrowed->copies == $request->copies){
                $borrowed->delete();
            }
            else
            {
                $borrowed->update(['copies' => $borrowed->copies - $request->copies]);
            }   
            
            $create_returned = returned_book::create($request->only(['book_id', 'copies', 'patron_id']));
            $returned = returned_book::with(['book'])->find($create_returned->id);
            $copies = $returned->book->copies + $request->copies;
            $returned->book->update(['copies' => $copies]);
            return response()->json(['returned_book' => $returned]);
        }  

    }
}
