<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePatronRequest;
use Illuminate\Http\Request;
use App\Models\Patron; 

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patron = Patron::all();
        return response()->json($patron); 
    }

    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePatronRequest $request)
    {
        $patron = Patron::create($request->validated());
        return response()->json(['patron' => $patron]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePatronRequest $request, $id)
    {
        Patron::where('id', $id)->update($request->validated());
        return response()->json(['patron' => Patron::where('id', $id)->first()]); 
    }

    /**
     * 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json(Patron::destroy($id)); 
    }
}
