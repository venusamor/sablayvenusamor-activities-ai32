<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookRequest;
use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        return response()->json(Book::with(['category:id,category'])->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookRequest $request)
    {
        $book = Book::create($request->all());
        return response()->json(['book' => $book]);
    }

    /**
     * 
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::where('id', $id)->firstOrFail();
        $book->delete();

        return response()->json(Book::destroy($id)); 
    }

    public function update(StoreBookRequest $request, $id)
    {
        $book = Book::where('id', $id)->firstOrFail();
        $book->update($request->validated());
        return response()->json(['book' => Book::where('id', $id)->firstOrFail()]);
    }

    
}
