<?php

Use Illuminate\Http\Request;
Use Illuminate\Support\Facades\Route;
Use App\Http\Controllers\CategoryController; 
Use App\Http\Controllers\BookController; 
Use App\Http\Controllers\PatronController; 
Use App\Http\Controllers\Borrowed_BookController;
Use App\Http\Controllers\Returned_BookController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('categories', CategoryController::class)->only('index');
Route::apiResource('books', BookController::class)->only(['index', 'store', 'update', 'destroy']);
Route::apiResource('borrowedbooks', Borrowed_BookController::class)->only(['index', 'store']);
Route::apiResource('patrons', PatronController::class)->only(['index', 'store', 'update', 'destroy']);
Route::apiResource('returnedbooks', Returned_BookController::class)->only(['index', 'store']);


